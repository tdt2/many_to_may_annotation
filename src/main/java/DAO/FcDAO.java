package DAO;

import HibernatePOJO.Fc;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class FcDAO {
    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public static FcDAO getInstance() {
        return new FcDAO();
    }

    public void save(Fc p) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(p);
            System.out.println("Save success !!!");
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }

    }

    public void showAll() {
        Session session = sessionFactory.openSession();
        List<Fc> list = session.createQuery("from Fc").list();
        list.forEach((Project) -> System.out.println(Project));
        session.close();
    }

    public void delete(Integer id) {
        Session session = sessionFactory.openSession();
        try{
            session.beginTransaction();
            Fc p=session.load(Fc.class,id);
            session.delete(p);
        }
        catch (RuntimeException e){
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            session.flush();
            session.close();
        }
    }


}
