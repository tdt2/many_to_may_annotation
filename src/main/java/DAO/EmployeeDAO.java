package DAO;

import HibernatePOJO.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class EmployeeDAO {
    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public static EmployeeDAO getInstance() {
        return new EmployeeDAO();
    }

    public void save(Employee em) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(em);
            System.out.println("Save Success!!!");
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }

    }

    public void showAll() {

        Session session = sessionFactory.openSession();
        List<Employee> list = session.createQuery("From Employee ").list();

        list.forEach((Employee) -> System.out.println(Employee));
        session.close();
    }
}
