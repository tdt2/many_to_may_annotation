package HibernatePOJO;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

@Entity(name = "Fc")
@Table(name = "fc")
public class Fc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer cash;
    @OneToMany(mappedBy = "fc", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FcEmp> listEmp;

    public void addEmp(Employee e) {
        FcEmp fe = new FcEmp(this, e);
        listEmp.add(fe);
        e.getListFc().add(fe);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCash() {
        return cash;
    }

    public void setCash(Integer cash) {
        this.cash = cash;
    }

    public List<FcEmp> getListEmp() {
        return listEmp;
    }

    public void setListEmp(List<FcEmp> listEmp) {
        this.listEmp = listEmp;
    }

    public Fc(String name, Integer cash) {
        this.name = name;
        this.cash = cash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fc)) return false;
        Fc fc = (Fc) o;
        return Objects.equals(id, fc.id) && Objects.equals(name, fc.name) && Objects.equals(cash, fc.cash) && Objects.equals(listEmp, fc.listEmp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cash, listEmp);
    }

    public Fc() {
    }

    @Override
    public String toString() {
        return "Fc{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cash=" + cash +
                '}';
    }
}