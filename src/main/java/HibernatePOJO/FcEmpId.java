package HibernatePOJO;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.beans.BeanInfo;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;

@Embeddable
public class FcEmpId implements Serializable {
    @Column(name = "fc_id")
    private Integer fcId;
    @Column(name = "emp_id")
    public Integer empId;

    public FcEmpId(Integer fcId, Integer empId) {
        this.fcId = fcId;
        this.empId = empId;
    }

    public FcEmpId() {
    }

    public Integer getFcId() {
        return fcId;
    }

    public void setFcId(Integer fcId) {
        this.fcId = fcId;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FcEmpId)) return false;
        FcEmpId fcEmpId = (FcEmpId) o;
        return Objects.equals(fcId, fcEmpId.fcId) && Objects.equals(empId, fcEmpId.empId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fcId, empId);
    }

    @Override
    public String toString() {
        return "FcEmpId{" +
                "fcId=" + fcId +
                ", empId=" + empId +
                '}';
    }
}