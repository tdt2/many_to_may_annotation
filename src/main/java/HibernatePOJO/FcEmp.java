package HibernatePOJO;

import HibernatePOJO.Employee;
import HibernatePOJO.Fc;
import HibernatePOJO.FcEmpId;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "fc_emp")

public class FcEmp {
    @EmbeddedId
    private FcEmpId id;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("fcId")
    @JoinColumn(name = "fc_id")
    private Fc fc;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("empId")
    private Employee emp;

    public FcEmp(Fc fc, Employee emp) {
        this.id = new FcEmpId(fc.getId(), emp.getId());
        this.fc = fc;
        this.emp = emp;
    }

    public FcEmpId getId() {
        return id;
    }

    public void setId(FcEmpId id) {
        this.id = id;
    }

    public Fc getFc() {
        return fc;
    }

    public void setFc(Fc fc) {
        this.fc = fc;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    public FcEmp() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FcEmp)) return false;
        FcEmp fcEmp = (FcEmp) o;
        return Objects.equals(id, fcEmp.id) && Objects.equals(fc, fcEmp.fc) && Objects.equals(emp, fcEmp.emp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fc, emp);
    }

    @Override
    public String toString() {
        return "FcEmp{" +
                "id=" + id +
                ", fc=" + fc +
                ", emp=" + emp +
                '}';
    }

//    public String showFc() {
//        return
//                "" + id +
//                ", fc=" + fc +
//                '}';
//    }
//
//
//    public String showEmp() {
//        return
//                "" + id +
//                ", emp=" + emp +
//                '}';
//    }
}
