package FPT.example;

import DAO.EmployeeDAO;

import DAO.FcDAO;
import HibernatePOJO.Employee;
import HibernatePOJO.Fc;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static Session getSession() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        return session;
    }

    public static void createFC() {
        Scanner sc = new Scanner(System.in);
        Fc f = new Fc();
        System.out.println("Nhập tên CLB:");
        f.setName(sc.nextLine());
        System.out.println("nhập giá trị CLB: ");
        f.setCash(sc.nextInt());
        FcDAO.getInstance().save(f);
    }

    public static void createEmp() {
        Scanner sc = new Scanner(System.in);
        Employee e = new Employee();
        System.out.println("Nhập tên cầu thủ:");
        e.setName(sc.nextLine());
        System.out.println("Nhập tuổi cầu thủ:");
        e.setAge(sc.nextInt());
        EmployeeDAO.getInstance().save(e);
    }

    public static void addEmp(Integer id) {
        Session session = getSession();
        session.beginTransaction();
        Fc f = new Fc();
//        f.setId(session.byId("Fc.class").load(id));
//        Employee e=session.byId("Employee.class").load(id);
    }

    static boolean flag = true;

    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        int a;
//
//        do {
//            System.out.println("Nhấn 1 để tạo CLB");
//            System.out.println("Nhấn 2 để tạo cầu thủ");
//            System.out.println("Nhấn phím bất kỳ để thoát");
//            a = sc.nextInt();
//            switch (a) {
//                case 1:
//                    System.out.println("Nhập thông tin CLB:");
//                    createFC();
//                    break;
//                case 2:
//                    System.out.println("Nhập thông tin cầu thủ:");
//                    createEmp();
//                    break;
//                case 3:
//                    System.out.println("Bị Lừa rồi, ko thoát được");
//                    break;
//                case 4:
//                    System.out.println("Bị Lừa rồi, ko thoát được");
//                    break;
//                case 5:
//                    System.out.println("Bị Lừa rồi, ko thoát được");
//                    break;
//                case 6:
//                    System.out.println("Bị Lừa rồi, ko thoát được");
//                    break;
//                default:
//                    System.out.println("Goodbye !!!");
//                    flag = false;
//            }
//        }
//        while (flag);
//
//    }
        Session session = getSession();
        try {
            session.beginTransaction();
         Fc f=session.load(Fc.class,5);
         Fc f1=session.load(Fc.class,1);
         Fc f2=session.load(Fc.class,6);
         Employee e=session.load(Employee.class,3);
         e.addFc(f);
         e.addFc(f1);
         e.addFc(f2);
            session.save(e);

            session.getTransaction().commit();

        } catch (RuntimeException e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        EmployeeDAO.getInstance().showAll();

    }
}